package com.mm.sag.model;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

public class User implements Serializable{
	private static final long serialVersionUID = 7902672399225619653L;
	
	private String uuid;
	private String email;
	private String password;
	private String status;
	private Date created;
	private Date updated;
	
	public User() {
		uuid = UUID.randomUUID().toString();
		Date now = new Date();
		created = now;
		updated = now;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	@Override
	public String toString() {
		return "User [uuid=" + uuid + ", email=" + email + ", password=" + password + ", status=" + status + "]";
	}
	
	
	
}
