package com.mm.sag.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mm.sag.repository.UserRepository;

@RestController
@RequestMapping("/")
public class IndexController {
	
	@Autowired
	private UserRepository userRepo;
	
	
	@RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json")
	public String test() {
		return userRepo.find("1").toString();
	}
}
