package com.mm.sag.repository;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.mm.sag.model.User;

@Mapper
public interface UserRepository {
	
	@Select("select * from user where uuid=#{uuid}")
	User find(String uuid);
	
	@Select("select * from user")
	List<User> findAll();
	
	@Insert("insert into user(uuid, email, password, status, created, updated "
			+ "values(#{uuid}, #{email}, #{password}, #{status}, #{created}, #{updated}))")
	int insert(User user);
	
	@Update("update user set email=#{email}, password=#{password}, status=#{status}, updated=#{updated}")
	int update(User user);
	
	@Delete("delete from user where uuid=#{uuid}")
	int delete(String uuid);
}
